###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01d - crazyCatLady - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build a crazyCatLady C++ program
###
### @author  Reid Lum <reidlum@hawaii.edu>
### @date    11 Jan 2022 
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = g++
CFLAGS = -g -Wall

TARGET = crazyCatLady

all: $(TARGET)

crazyCatLady: crazyCatLady.cpp
	$(CC) $(CFLAGS) -o $(TARGET) crazyCatLady.cpp

clean:
	rm -f $(TARGET) *.o

